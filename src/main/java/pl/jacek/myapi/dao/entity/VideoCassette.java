package pl.jacek.myapi.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class VideoCassette {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id

    private Long id;
    private String title;
    private LocalDate produtionYear;

    public VideoCassette() {

    }

    public VideoCassette(Long id, String title, LocalDate produtionYear) {
        this.id = id;
        this.title = title;
        this.produtionYear = produtionYear;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getProdutionYear() {
        return produtionYear;
    }

    public void setProdutionYear(LocalDate produtionYear) {
        this.produtionYear = produtionYear;
    }
}
