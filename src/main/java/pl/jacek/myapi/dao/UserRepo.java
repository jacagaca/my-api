package pl.jacek.myapi.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jacek.myapi.dao.entity.User;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {

}
