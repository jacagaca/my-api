package pl.jacek.myapi.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.jacek.myapi.dao.entity.User;
import pl.jacek.myapi.manager.UserManager;

import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserApi {

    private UserManager users;

    @Autowired
    public UserApi(UserManager users) {
        this.users = users;
    }


    @GetMapping("/all")
    public Iterable<User> getAll() {
        return users.findAll();
    }

    @GetMapping
    public Optional<User> getById(@RequestParam long id) {
        return users.findById(id);
    }


    @PostMapping
    public User addVideo(@RequestBody User user) {
        return users.save(user);
    }

    @PutMapping
    public User updateVideo(@RequestBody User user) {
        return users.save(user);
    }

    @DeleteMapping
    public void deleteById(@RequestParam long id) {
        users.deleteById(id);
    }
}