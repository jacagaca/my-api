package pl.jacek.myapi.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.jacek.myapi.dao.UserRepo;
import pl.jacek.myapi.dao.entity.User;
import pl.jacek.myapi.dao.entity.VideoCassette;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UserManager {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public Optional<User> findById(Long id) {
        return userRepo.findById(id);
    }

    public Iterable<User> findAll() {
        return userRepo.findAll();
    }

    public User save(User user) {
        return userRepo.save(user);
    }

    public void deleteById(Long id) {
        userRepo.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        LocalDate date = LocalDate.now();
        save(new User(1L, "Jacek", "jacek@o2.pl", "lol123", date));
        save(new User(2L, "Adam", "adam@o2.pl", "lol321", date));
    }

}